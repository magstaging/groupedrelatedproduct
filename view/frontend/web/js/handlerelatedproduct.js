define([
    'jquery'
], function($) {
   'use strict';

   return function (config, element) {
       var productListContainer = $('.related .col-9 ul');
       var categoryLinkAnchor = $('.related .col-3 span');

       renderProductForCategory(config.firstCategorySlug);

       categoryLinkAnchor.on('click', function () {
            var categorySlug = $(this).attr('data-slug');
            renderProductForCategory(categorySlug);
        });

       function renderProductForCategory(categorySlug) {
           productListContainer.each(function (categoryKey, categoryItems) {
               if ($(categoryItems).hasClass(categorySlug)) {
                   $(categoryItems).show();
               } else {
                   $(categoryItems).hide();
               }
           });

           categoryLinkAnchor.each(function (categoryKey, categoryInfo) {
               if ($(categoryInfo).attr('data-slug') === categorySlug) {
                   $(categoryInfo).addClass('active');
               } else {
                   $(categoryInfo).removeClass('active');
               }
           })
       }
   }
});
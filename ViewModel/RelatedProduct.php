<?php

namespace Mbs\ProductRelated\ViewModel;

use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class RelatedProduct implements ArgumentInterface
{
    /**
     * @var \Magento\Framework\Data\CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    private $dataObjectFactory;
    /**
     * @var \Mbs\ProductRelated\Model\CategoryInfoFinder
     */
    private $categoryInfoFinder;

    private $firstCategorySlug = null;

    public function __construct(
        \Magento\Framework\Data\CollectionFactory $collectionFactory,
        \Magento\Framework\DataObjectFactory $dataObjectFactory,
        \Mbs\ProductRelated\Model\CategoryInfoFinder $categoryInfoFinder
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->categoryInfoFinder = $categoryInfoFinder;
    }

    public function getItemsGroupedByCategory(Collection $items)
    {
        $items = $this->categoryInfoFinder->addCategoryInfo($items);

        $categoryList = [];
        foreach ($items as $product) {
            $categoryList[] = $product->getCategorySlug();
            $this->setFirstCategorySlug($product->getCategorySlug());
        }
        $categoryList = array_unique($categoryList);

        $collection = $this->collectionFactory->create();

        foreach ($categoryList as $categorySlug) {
            $productsInCategory = $items->getItemsByColumnValue('category_slug', $categorySlug);

            $categoryItems = [];
            foreach ($productsInCategory as $product) {
                $categoryItems[] = $product;
            }

            $categoryItemData = $this->dataObjectFactory->create();
            $categoryItemData->setCategoryName($product->getCategoryName());
            $categoryItemData->setCategoryItems($categoryItems);
            $categoryItemData->setCategorySlug($categorySlug);

            $collection->addItem($categoryItemData);
        }

        return $collection;
    }

    public function getFirstCategorySlug()
    {
        return $this->firstCategorySlug;
    }

    private function setFirstCategorySlug($categorySlug)
    {
        if (is_null($this->firstCategorySlug)) {
            $this->firstCategorySlug = $categorySlug;
        }
    }
}
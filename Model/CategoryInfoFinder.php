<?php

namespace Mbs\ProductRelated\Model;

use Magento\Store\Model\Store;

class CategoryInfoFinder
{
    /**
     * @var \Magento\Eav\Model\Config
     */
    private $eavConfig;

    public function __construct(
        \Magento\Eav\Model\Config $eavConfig
    ) {
        $this->eavConfig = $eavConfig;
    }

    public function addCategoryInfo(\Magento\Catalog\Model\ResourceModel\Product\Collection $items)
    {
        $productIds = [];
        foreach ($items as $product) {
            $productIds[] = $product->getId();
        }

        $select = $items->getSelect()->getConnection()->select()->from(['category_product' => 'catalog_category_product'])
            ->where('product_id in (?)', $productIds)
            ->reset(\Magento\Framework\DB\Select::COLUMNS)
            ->columns(['product_id' => 'category_product.product_id']);

        $categoryNameAttribute = $this->eavConfig->getAttribute(\Magento\Catalog\Model\Category::ENTITY, 'name');

        $cond[] = "category_name.entity_id=category_product.category_id";
        $cond[] = $items->getSelect()->getConnection()->quoteInto("category_name.attribute_id=?", $categoryNameAttribute->getId());
        $cond[] = $items->getSelect()->getConnection()->quoteInto("category_name.store_id=?", Store::DEFAULT_STORE_ID);
        $select = $select->joinLeft(
            ['category_name' => 'catalog_category_entity_' . $categoryNameAttribute->getBackendType()],
            implode(' AND ', $cond),
            ['category_name' => 'value', 'category_id' => 'category_name.entity_id']
        );

        $select->order('category_product.category_id ' . \Magento\Framework\Data\Collection::SORT_ORDER_DESC);

        $categoryInfoList = $items->getSelect()->getConnection()->fetchAll($select);

        foreach ($items as $product) {
            $categoryInfo = $this->findCategoryInfoForProduct($product, $categoryInfoList);

            if ($categoryInfo) {
                $product->setCategoryName($categoryInfo['category_name']);
                $product->setCategorySlug('category-' . $categoryInfo['category_id']);
            }
        }

        return $items;
    }

    /**
     * @param $product
     * @param array $categoryInfoList
     * @return bool|mixed
     */
    private function findCategoryInfoForProduct($product, array $categoryInfoList)
    {
        foreach ($categoryInfoList as $categoryInfo) {
            if ($categoryInfo['product_id'] == $product->getId()) {
                return $categoryInfo;
            }
        }

        return false;
    }
}

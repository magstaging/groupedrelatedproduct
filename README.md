# README #

Group related product by category

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/ProductRelated when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

Go to product detail page ensuring some related products do exists for the visited product. The listing should appear grouped 
by category